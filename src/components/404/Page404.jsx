import { Empty, Result } from "antd";
import React from "react";

export default function Page404() {
  return (
    <Result
      className="mt-10"
      status="404"
      title="404"
      subTitle="Sorry, the page you visited does not exist."
    />
  );
}
