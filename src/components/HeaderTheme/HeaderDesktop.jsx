import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";

export default function HeaderDesktop() {
  return (
    <div className="h-12 w-full flex items-center justify-between shadow-lg px-5 ">
      <NavLink to={"/"} className="text-2xl font-bold h-full text-orange-500">
        <img
          src="https://dehayf5mhw1h7.cloudfront.net/wp-content/uploads/sites/757/2022/03/26135251/Netflix-Brand-Logo.png"
          alt=""
          className="h-full"
        />
      </NavLink>

      <div className=" space-x-5 text-md font-medium text-gray-600 ">
        <NavLink to={"/"} className="hover:text-red-600">
          <span>Trang chủ</span>
        </NavLink>
        <NavLink to={"/"} className="hover:text-red-600">
          <span>Cụm rạp</span>
        </NavLink>
        <NavLink to={"/"} className="hover:text-red-600">
          <span>Tin tức</span>
        </NavLink>
      </div>

      <UserNav />
    </div>
  );
}
