import {
  LogoutOutlined,
  LoginOutlined,
  FormOutlined,
  UserOutlined,
} from "@ant-design/icons";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localStorageService } from "../../services/localStorageService";
import { onClickLogin } from "../../redux/actions/userAction";

export default function UserNav() {
  let userInfo = useSelector((state) => state.userReducer.userInfo);
  let dispatch = useDispatch();

  const handleLoginOnClick = () => {
    dispatch(onClickLogin(window.location.pathname));
  };

  return (
    <div className="text-md text-md font-medium text-gray-600">
      {userInfo ? (
        <div className=" space-x-3 md:flex md:pb-0 md:mb-0 items-center md:border-none border-b-2 border-b-slate-300 pb-4 mb-4">
          <NavLink to={"/profile"} className="hover:text-red-600">
            <div className="flex items-center">
              <UserOutlined className="font-bold" />
              <span className="ml-1 font-bold">{userInfo.hoTen}</span>
            </div>
          </NavLink>
          <NavLink to={"/"} className="hover:text-red-600">
            <div className="flex items-center">
              <LogoutOutlined />
              <span
                className="ml-1"
                onClick={() => {
                  localStorageService.removeUserInfo();
                  window.location.href = "/";
                }}
              >
                Đăng xuất
              </span>
            </div>
          </NavLink>
        </div>
      ) : (
        <div className="space-x-3 md:flex md:pb-0 md:mb-0 items-center md:border-none border-b-2 border-b-slate-300 pb-4 mb-4">
          <NavLink
            to={"/login"}
            className=" hover:text-red-600"
            onClick={() => handleLoginOnClick()}
          >
            <div className="flex items-center">
              <LoginOutlined /> <span className="ml-1">Đăng nhập</span>
            </div>
          </NavLink>
          <NavLink to={"/register"} className="hover:text-red-600">
            <div className="flex items-center">
              {" "}
              <FormOutlined /> <span className="ml-1">Đăng kí</span>
            </div>
          </NavLink>
        </div>
      )}
    </div>
  );
}
