import { MenuOutlined } from "@ant-design/icons";
import { Drawer } from "antd";
import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";

export default function HeaderMobile() {
  const [visible, setVisible] = useState(false);

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };
  return (
    <>
      <Drawer
        title="Menu"
        placement="right"
        onClose={onClose}
        visible={visible}
      >
        <UserNav />
        <div className=" space-x-5 text-md font-medium text-gray-600 ">
          <NavLink to={"/"} className="hover:text-red-600">
            <p>Trang chủ</p>
          </NavLink>
          <NavLink to={"/"} className="hover:text-red-600">
            <p>Cụm rạp</p>
          </NavLink>
          <NavLink to={"/"} className="hover:text-red-600">
            <p>Tin tức</p>
          </NavLink>
        </div>
      </Drawer>
      <div className="h-12 w-full flex items-center justify-between shadow-lg px-5 ">
        <NavLink to={"/"} className="text-2xl font-bold h-full text-orange-500">
          <img
            src="https://dehayf5mhw1h7.cloudfront.net/wp-content/uploads/sites/757/2022/03/26135251/Netflix-Brand-Logo.png"
            alt=""
            className="h-full"
          />
        </NavLink>
        <MenuOutlined
          className="text-red-500 text-xl hover:cursor-pointer"
          onClick={showDrawer}
        />
      </div>
    </>
  );
}
