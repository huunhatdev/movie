import React from "react";
import useWindowSize from "../../Hook/useWindowSize";
import HeaderDesktop from "./HeaderDesktop";
import HeaderMobile from "./HeaderMobile";

export default function HeaderTheme() {
  let windowSize = useWindowSize();

  let renderHeader = () => {
    if (windowSize.width > 768) return <HeaderDesktop />;
    return <HeaderMobile />;
  };
  return <>{renderHeader()}</>;
}
