import React, { useState } from "react";
import ReactDOM from "react-dom";
import ModalVideo from "react-modal-video";
import { useDispatch, useSelector } from "react-redux";
import { onCloseTrailer } from "../../redux/actions/userAction";

export default function VideoOverplay() {
  let trailer = useSelector((state) => state.userReducer.trailer);
  let dispatch = useDispatch();

  return (
    <ModalVideo
      channel="youtube"
      autoplay
      isOpen={trailer.isOpen}
      videoId={trailer.id}
      onClose={() => dispatch(onCloseTrailer())}
    />
  );
}
