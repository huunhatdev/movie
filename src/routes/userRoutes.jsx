import Page404 from "../components/404/Page404";
import LayoutTheme from "../HOC/Layout";
import DetaiPage from "../pages/DetailPage/DetaiPage";
import HomePage from "../pages/HomePage/HomePage";
import LoginPage from "../pages/LoginPage/LoginPage";
import ProfilePage from "../pages/Profile/ProfilePage";
import PurchasePage from "../pages/Purchase/PurchasePage";
import RegisterPage from "../pages/RegisterPage/RegisterPage";

export const userRoutes = [
  {
    path: "/",
    component: <LayoutTheme Component={HomePage} />,
    exact: true,
    isUseLayout: true,
  },
  {
    path: "/login",
    component: LoginPage,
  },
  {
    path: "/register",
    component: RegisterPage,
  },
  {
    path: "/detail/:id",
    component: <LayoutTheme Component={DetaiPage} />,
    isUseLayout: true,
  },
  {
    path: "/purchase/:maLichChieu",
    component: <LayoutTheme Component={PurchasePage} />,
    isUseLayout: true,
  },
  {
    path: "/profile",
    component: <LayoutTheme Component={ProfilePage} />,
    isUseLayout: true,
  },
  {
    path: "*",
    component: <LayoutTheme Component={Page404} />,
    isUseLayout: true,
  },
];
