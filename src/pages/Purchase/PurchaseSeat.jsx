import _ from "lodash";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setSelectSeat } from "../../redux/actions/userAction";

export default function PurchaseSeat({ dataSeat, showtime }) {
  const [seats, setSeats] = useState([]);
  let select = useSelector((state) => state.userReducer.userSelect);

  useEffect(() => {
    let chunkedList = _.chunk(dataSeat, 16);
    setSeats(chunkedList);
  }, [select]);

  const checkSelected = (maGhe) => {
    let flag = false;
    select.length &&
      select.forEach((e) => {
        if (e.maGhe === maGhe) {
          flag = true;
        }
      });
    return flag;
  };

  const handleSelect = useCallback(
    (seat) => {
      dispatch(setSelectSeat(seat, showtime));
    },
    [select]
  );

  const dispatch = useDispatch();

  return (
    <>
      {seats.map((row, iRow) => {
        return (
          <div
            key={iRow}
            className="flex sm:space-x-1 md:space-x-2 mb-2 w-full sm:text-xs "
          >
            {row.map((seat, iSeat) => {
              if (seat.daDat) {
                return (
                  <span
                    key={iSeat}
                    className="seat inline-block w-10 h-10  bg-gray-500 rounded text-center leading-10"
                  >
                    x
                  </span>
                );
              }
              if (checkSelected(seat.maGhe)) {
                return (
                  <span
                    key={iSeat}
                    className="seat inline-block w-10 h-10 bg-green-500 rounded text-center leading-10 text-white cursor-pointer"
                    onClick={() => handleSelect(seat)}
                  >
                    {seat.tenGhe}
                  </span>
                );
              }

              if (seat.loaiGhe === "Vip") {
                return (
                  <span
                    key={iSeat}
                    className="seat inline-block w-10 h-10 bg-orange-500 rounded text-center leading-10 text-white cursor-pointer"
                    onClick={() => handleSelect(seat)}
                  >
                    {seat.tenGhe}
                  </span>
                );
              }
              return (
                <span
                  key={iSeat}
                  className="seat inline-block w-10 h-10 bg-gray-100 rounded text-center leading-10 cursor-pointer"
                  onClick={() => handleSelect(seat)}
                >
                  {seat.tenGhe}
                </span>
              );
            })}
          </div>
        );
      })}

      {/* info seat */}
      <div className="flex justify-center space-x-5 mt-5">
        <div className=" flex-col items-center justify-center">
          <div className="mb-1 seat seat--info w-10 h-10  bg-gray-500 rounded text-center leading-10 mx-auto">
            X
          </div>
          <p>Đã đặt</p>
        </div>
        <div className="flex-col items-center justify-center">
          <div className="mb-1 mx-auto seat seat--info w-10 h-10 bg-orange-500 rounded text-center leading-10 text-white cursor-pointer">
            0
          </div>
          <p>Ghế VIP</p>
        </div>
        <div className="flex-col items-center justify-center">
          <div className="mb-1 mx-auto seat seat--info w-10 h-10 bg-gray-100 rounded text-center leading-10 cursor-pointer">
            0
          </div>
          <p>Ghế thường</p>
        </div>
        <div className="flex-col items-center justify-center">
          <div className="mb-1 mx-auto seat seat--info w-10 h-10 bg-green-500 rounded text-center leading-10 text-white cursor-pointer">
            0
          </div>
          <p>Ghế chọn</p>
        </div>
      </div>
    </>
  );
}
