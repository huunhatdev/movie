import { Spin } from "antd";
import React, { useState } from "react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { clearSelect } from "../../redux/actions/userAction";
import { movieService } from "../../services/movieService";
import PurchaseDetail from "./PurchaseDetail";
import PurchaseSeat from "./PurchaseSeat";
import "./purchaseStyle.css";

export default function PurchasePage() {
  let { maLichChieu } = useParams();
  const [data, setData] = useState({});
  const [spin, setSpin] = useState(true);

  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(clearSelect());

    movieService
      .getListSeat(maLichChieu)
      .then((res) => {
        setData(res.data.content);
        setSpin(false);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div className="container mx-auto relative w-full">
      <Spin
        size="large"
        spinning={spin}
        style={{ position: "absolute", top: "50%", left: "50%" }}
      />
      <div className="lg:grid lg:grid-cols-3  gap-4 mt-10 w-full">
        {data.hasOwnProperty("danhSachGhe") && (
          <div className="w-full lg:col-span-2 pb-5">
            <PurchaseSeat
              dataSeat={data.danhSachGhe}
              showtime={data.thongTinPhim.maLichChieu}
            />
          </div>
        )}
        {data.hasOwnProperty("thongTinPhim") && (
          <PurchaseDetail thongTinPhim={data.thongTinPhim} />
        )}
      </div>
    </div>
  );
}
