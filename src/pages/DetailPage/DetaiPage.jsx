import { Button, Empty, Progress, Rate, Tag } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { onOpenTrailer } from "../../redux/actions/userAction";
import { movieService } from "../../services/movieService";
import DetailTabs from "./DetailTabs/DetailTabs";
import moment from "moment";
import "./detailStyle.css";

export default function DetaiPage() {
  let { id } = useParams();
  const [movie, setMovie] = useState({});
  const [rate, setRate] = useState(3);

  const desc = ["terrible", "bad", "normal", "good", "wonderful"];
  const dispatch = useDispatch();
  useEffect(() => {
    movieService
      .getDetailMovie(id)
      .then((res) => {
        console.log(res.data.content);
        setMovie(res.data.content);
        setRate((res.data.content.danhGia / 10) * 5);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="container mx-auto py-10 space-y-10">
      {!movie.hasOwnProperty("tenPhim") ? (
        <Empty />
      ) : (
        <>
          <div className="md:flex justify-between space-x-5 px-5">
            <div className="w-full px-5 rounded md:w-80 md:px-0 mb-4 md:mb-0">
              <img src={movie.hinhAnh} className="w-full rounded-lg" alt="" />
            </div>
            <div className="flex-col justify-between relative w-full">
              {movie.hot ? <Tag color="red">Hot</Tag> : ""}
              {movie.sapChieu ? <Tag color="geekblue">Sắp chiếu</Tag> : ""}
              {movie.dangChieu ? <Tag color="green">Đang chiếu</Tag> : ""}
              <p className="text-2xl my-2">{movie.tenPhim}</p>
              <p className="detail__desc">{movie.moTa}</p>
              <p className="my-2">
                Ngày khởi chiếu:{" "}
                {moment(movie.ngayKhoiChieu).format("DD/MM/YYYY")}
              </p>
              <p>Đánh giá: {movie.danhGia} / 10</p>
              <div className="space-x-2 md:absolute bottom-0 mt-2">
                <button
                  className="inline-block px-4 py-2 bg-green-500 text-white font-medium text-xs leading-tight uppercase rounded-sm shadow-md hover:bg-green-600 hover:shadow-lg focus:bg-green-600 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-green-700 active:shadow-lg transition duration-150 ease-in-out"
                  onClick={() => dispatch(onOpenTrailer(movie.trailer))}
                >
                  Trailer
                </button>
                <a
                  href="#detail__theater"
                  className="inline-block px-4 py-2 bg-blue-400 text-white font-medium text-xs leading-tight uppercase rounded-sm shadow-md hover:bg-blue-500 hover:shadow-lg focus:bg-blue-500 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-600 active:shadow-lg transition duration-150 ease-in-out"
                >
                  Mua vé
                </a>
              </div>
            </div>
            <div
              className="flex-col items-center justify-center md:block hidden"
              id="rate"
            >
              <div>
                <Progress
                  type="circle"
                  percent={(movie.danhGia / 10) * 100}
                  width={200}
                  strokeColor={{
                    "0%": "#108ee9",
                    "100%": "#87d068",
                  }}
                  strokeWidth={10}
                  format={() => {
                    return (
                      <span className="text-blue-600">
                        {movie.danhGia} điểm
                      </span>
                    );
                  }}
                />
              </div>
              <div className="flex justify-center mt-5">
                <Rate tooltips={desc} onChange={setRate} value={rate} />
              </div>
            </div>
          </div>
          <div id="detail__theater">
            <DetailTabs movie={movie} />
          </div>
        </>
      )}
    </div>
  );
}
