import { message } from "antd";
import React, { useEffect } from "react";
import { useState } from "react";
import { userServices } from "../../services/userService";

export default function ProfileDetail({ profile }) {
  const [info, setInfo] = useState({});
  useEffect(() => {
    setInfo(profile);
  }, []);

  const handleOnChange = (e) => {
    let { name, value } = e.target;
    setInfo({ ...info, [name]: value });
  };

  const handleUpdateProfile = () => {
    userServices
      .updateProfile(info)
      .then((res) => {
        message.success(res.data.message);
        setInfo(res.data.content);
      })
      .catch((err) => {
        message.success(err.response.data.content);

        console.log(err);
      });
  };

  return (
    <div className=" shadow-2xl rounded-lg p-5 mb-10">
      <h2 className=" text-2xl border-b-2 border-b-slate-200 pb-4 mb-4">
        Thông tin cá nhân
      </h2>
      <div className=" grid lg:grid-cols-3 sm:grid-cols-2 xs:grid-cols-1 gap-2 ">
        <div class="w-full">
          <div class="mb-3">
            <label
              for="username"
              class="form-label inline-block mb-2 text-gray-700"
            >
              Tài khoản
            </label>
            <input
              required
              value={info.taiKhoan}
              type="text"
              class="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
              id="username"
              name="taiKhoan"
              placeholder="Tài khoản"
              disabled
            />
          </div>
        </div>
        <div class="w-full">
          <div class="mb-3">
            <label
              for="password"
              class="form-label inline-block mb-2 text-gray-700"
            >
              Mật khẩu
            </label>
            <input
              onChange={(e) => handleOnChange(e)}
              required
              value={info.matKhau}
              type="password"
              class="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
              id="password"
              name="matKhau"
              placeholder="Mật khẩu"
            />
          </div>
        </div>
        <div class="w-full">
          <div class="mb-3">
            <label
              for="name"
              class="form-label inline-block mb-2 text-gray-700"
            >
              Họ tên
            </label>
            <input
              onChange={(e) => handleOnChange(e)}
              value={info.hoTen}
              type="text"
              class="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
              id="name"
              name="hoTen"
              placeholder="Họ tên"
            />
          </div>
        </div>
        <div class="w-full">
          <div class="mb-3">
            <label
              for="email"
              class="form-label inline-block mb-2 text-gray-700"
            >
              Email
            </label>
            <input
              onChange={(e) => handleOnChange(e)}
              value={info.email}
              type="email"
              class="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
              id="email"
              name="email"
              placeholder="Email"
            />
          </div>
        </div>
        <div class="w-full">
          <div class="mb-3">
            <label
              for="phone"
              class="form-label inline-block mb-2 text-gray-700"
            >
              Số điện thoại
            </label>
            <input
              onChange={(e) => handleOnChange(e)}
              value={info.soDT}
              type="phone"
              class="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
              id="phone"
              name="soDT"
              placeholder="Số điện thoại"
            />
          </div>
        </div>
        <div class="w-full">
          <div class="mb-3">
            <label
              for="phone"
              class="form-label inline-block mb-2 text-gray-700"
            >
              Loại người dùng
            </label>
            <select
              name=""
              class="form-select appearance-none
      block
      w-full
      px-3
      py-1.5
      text-base
      font-normal
      text-gray-700
      bg-white bg-clip-padding bg-no-repeat
      border border-solid border-gray-300
      rounded
      transition
      ease-in-out
      m-0
      focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
              aria-label="Default select example"
            >
              <option
                selected={info.maLoaiNguoiDung === "KhachHang"}
                value="KhachHang"
              >
                Khách hàng
              </option>
              <option
                selected={info.maLoaiNguoiDung === "QuanTri"}
                value="QuanTri"
              >
                Quản trị
              </option>
            </select>
          </div>
        </div>
        <div className=" lg:col-span-3 sm:col-span-2 xs:col-span-1 flex justify-end">
          <button
            onClick={handleUpdateProfile}
            className="inline-block px-6 py-2.5 bg-red-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-red-700 hover:shadow-lg focus:bg-red-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-red-800 active:shadow-lg transition duration-150 ease-in-out"
          >
            Cập nhật
          </button>
        </div>
      </div>
    </div>
  );
}
