import React, { useEffect, useState } from "react";
import { userServices } from "../../services/userService";
import ProfileDetail from "./ProfileDetail";
import ProfileTicketHistory from "./ProfileTicketHistory";

export default function ProfilePage() {
  const [profile, setProfile] = useState([]);
  useEffect(() => {
    userServices
      .getProfile()
      .then((res) => {
        setProfile(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="container mx-auto">
      {profile.length !== 0 && (
        <>
          <ProfileDetail profile={profile} />
          <ProfileTicketHistory tickets={profile.thongTinDatVe} />
        </>
      )}
    </div>
  );
}
