import React from "react";
import moment from "moment";
// import "./profilePage.css";s

export default function ProfileTicketHistory({ tickets }) {
  return (
    <div className=" shadow-2xl rounded-lg p-5">
      <h2 className=" text-2xl border-b-2 border-b-slate-200 pb-4 mb-4">
        Lịch sử đặt vé
      </h2>
      <div className=" grid lg:grid-cols-3 sm:grid-cols-2 xs:grid-cols-1 gap-4 ">
        {tickets.map((e, i) => {
          return (
            <div class="cardWrap my-2 w-full rounded-lg overflow-hidden shadow-lg pb-2">
              <div class="card cardLeft ">
                <h1 className="text-xl bg-red-600 text-white p-2">
                  {e.danhSachGhe[0].tenHeThongRap}
                </h1>
                <div class="title pl-2">
                  <h2 className="text-lg font-medium">{e.tenPhim}</h2>
                  <span className="">Movie</span>
                </div>
                <div class="pl-2 flex space-x-5 my-2">
                  <div className="date">
                    <h2 className="text-lg font-medium">
                      {moment(e.ngayDat).format("DD/MM/YYYY HH:MM")}
                    </h2>
                    <span>Date</span>
                  </div>
                  <div class="price">
                    <h2 className="text-lg font-medium">{e.giaVe}</h2>
                    <span>Giá vé</span>
                  </div>
                </div>
                <div className="pl-2 flex space-x-5">
                  <div class="seat">
                    <h2 className="text-lg font-medium">
                      {e.danhSachGhe[0].tenRap}
                    </h2>
                    <span>Rạp</span>
                  </div>
                  <div class="theater">
                    <h2 className="text-lg font-medium">
                      {e.danhSachGhe.map((e, i) => `${e.tenGhe} `)}
                    </h2>
                    <span>Ghế</span>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
