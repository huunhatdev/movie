import { Button, Checkbox, Form, Input, message } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  setUserAction,
  setUserInfoActionServ,
} from "../../../redux/actions/userAction";
import { localStorageService } from "../../../services/localStorageService";
import { userServices } from "../../../services/userService";

export default function FormLogin() {
  //can thiệp thanh url, dùng để chuyển hướng trang
  let history = useHistory();
  let dispatch = useDispatch();
  let pathName = useSelector((state) => state.userReducer.pathNameLogin);

  const onFinish = (values) => {
    let onSuccess = (res) => {
      localStorageService.setUserInfo(res);
      message.success("Đăng nhập thành công");
      setTimeout(() => {
        history.push(pathName);
      }, 2000);
    };

    let onFail = (msg) => {
      message.error(msg);
    };
    dispatch(setUserInfoActionServ(values, onSuccess, onFail));
  };

  const onFinishFailed = (errorInfo) => {};

  return (
    <Form
      name="basic"
      layout="vertical"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label={<label style={{ color: "white" }}>Tên đăng nhập</label>}
        name="taiKhoan"
        rules={[
          {
            required: true,
            message: "Nhập tài khoản!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label={<label style={{ color: "white" }}>Mật khẩu</label>}
        name="matKhau"
        rules={[
          {
            required: true,
            message: "Nhập mật khẩu!",
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item>
        <Button
          className=" h-10 mt-3 w-full"
          type="primary"
          htmlType="submit"
          block
          danger
        >
          Đăng nhập
        </Button>
      </Form.Item>
      <Form.Item className="text-center text-white ">
        Chưa có tài khoản?
        <a
          className=" text-blue-600 underline mx-2"
          onClick={() => {
            history.push("/register");
          }}
        >
          Đăng kí ngay
        </a>
      </Form.Item>
    </Form>
  );
}
