import React from "react";
import FormLogin from "./FormLogin/FormLogin";
import "../css/styleLoginPage.css";
import { NavLink } from "react-router-dom";

export default function LoginPage() {
  return (
    <div className="wrapper-login w-screen h-screen relative">
      <div className="bg-login w-screen h-screen absolute"></div>
      <NavLink to={"/"} className="w-full flex z-10 h-28 relative">
        <img
          src="https://dehayf5mhw1h7.cloudfront.net/wp-content/uploads/sites/757/2022/03/26135251/Netflix-Brand-Logo.png"
          alt=""
        />
      </NavLink>
      <div className="form-login relative container mx-auto  p-5 rounded bg-black/50 z-10 w-full">
        <h2>Đăng nhập</h2>
        <FormLogin />
      </div>
    </div>
  );
}
