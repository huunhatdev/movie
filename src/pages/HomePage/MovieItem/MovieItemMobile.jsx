import { Card, Button } from "antd";
import moment from "moment";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { onOpenTrailer } from "../../../redux/actions/userAction";
const { Meta } = Card;

export default function MovieItemMobile({ movieItem }) {
  let dispatch = useDispatch();
  return (
    <>
      <div className="grid grid-cols-2 gap-1 relative mb-4">
        <div className="poster">
          <img
            className="w-full"
            src={movieItem.hinhAnh}
            alt=""
            onClick={() => dispatch(onOpenTrailer(movieItem.trailer))}
          />
        </div>
        <div className="content">
          <h3 className="font-bold">{movieItem.tenPhim}</h3>
          <p className=" text-sm text-red-500">
            <span>Ngày: </span>
            {moment(movieItem.ngayKhoiChieu).format("DD/MM/yyyy")}
          </p>
          <p className="homePage__desc--mobile">{movieItem.moTa}</p>
          <div className="mt-2 absolute bottom-0 right-0 space-x-1">
            <NavLink to={`/detail/${movieItem.maPhim}`}>
              <button className="inline-block px-4 py-2 bg-red-500 text-white font-medium text-xs leading-tight uppercase rounded-sm shadow-md hover:bg-red-400 hover:shadow-lg focus:bg-red-400 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-red-600 active:shadow-lg transition duration-150 ease-in-out">
                Chi tiết
              </button>
            </NavLink>
          </div>
        </div>
      </div>
    </>
  );
}
