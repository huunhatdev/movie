import { Card, Button } from "antd";
import moment from "moment";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { onOpenTrailer } from "../../../redux/actions/userAction";
const { Meta } = Card;

export default function MovieItemDesktop({ movieItem }) {
  let dispatch = useDispatch();
  return (
    <Card
      // loading={true}
      hoverable
      style={{
        width: "100%",
        paddingBottom: "30px",
        borderRadius: "5px",
        overflow: "hidden",
      }}
      cover={
        <div className="w-full h-96 overflow-hidden">
          <img
            alt="example"
            src={movieItem.hinhAnh}
            className=" hover:scale-110 w-full"
            style={{ objectFit: "cover", transition: "0.1s" }}
          />
        </div>
      }
    >
      <Meta
        title={movieItem.tenPhim}
        description={
          <>
            <p className="text-red-500">
              <span>Ngày khởi chiếu: </span>
              {moment(movieItem.ngayKhoiChieu).format("DD/MM/yyyy")}
            </p>
            <p className="homePage__desc">{movieItem.moTa}</p>
          </>
        }
      />
      <div className="mt-2 absolute bottom-2 right-2 space-x-1">
        <button
          className="inline-block px-4 py-2 bg-green-500 text-white font-medium text-xs leading-tight uppercase rounded-sm shadow-md hover:bg-green-600 hover:shadow-lg focus:bg-green-600 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-green-700 active:shadow-lg transition duration-150 ease-in-out"
          onClick={() => dispatch(onOpenTrailer(movieItem.trailer))}
        >
          Trailer
        </button>
        <NavLink to={`/detail/${movieItem.maPhim}`}>
          <button className="inline-block px-4 py-2 bg-red-500 text-white font-medium text-xs leading-tight uppercase rounded-sm shadow-md hover:bg-red-400 hover:shadow-lg focus:bg-red-400 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-red-600 active:shadow-lg transition duration-150 ease-in-out">
            Chi tiết
          </button>
        </NavLink>
      </div>
    </Card>
  );
}
