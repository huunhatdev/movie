import React from "react";
import useWindowSize from "../../../Hook/useWindowSize";
import MovieItemDesktop from "./MovieItemDesktop";
import MovieItemMobile from "./MovieItemMobile";

export default function MovieItem({ movieItem }) {
  let windowSize = useWindowSize();

  let renderHeader = () => {
    //   if (windowSize.width > 992) return <MovieItemDesktop />;
    if (windowSize.width > 768)
      return <MovieItemDesktop movieItem={movieItem} />;
    return <MovieItemMobile movieItem={movieItem} />;
  };
  return <div>{renderHeader()}</div>;
}
