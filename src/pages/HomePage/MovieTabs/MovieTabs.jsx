import { Radio, Space, Tabs } from "antd";
import { useEffect, useState } from "react";
import useWindowSize from "../../../Hook/useWindowSize";
import { movieService } from "../../../services/movieService";
import MovieTabItem from "./MovieTabItem";
const { TabPane } = Tabs;

const MovieTabs = () => {
  const [tabPosition, setTabPosition] = useState("left");
  const [dataRap, setDataRap] = useState([]);
  const heightTab = 600;
  let windowSize = useWindowSize();

  useEffect(() => {
    movieService
      .getMovieByTheater("GP03")
      .then((res) => {
        setDataRap(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const changeTabPosition = (e) => {
    setTabPosition(e.target.value);
  };

  let renderTabContent = () => {
    return dataRap.map((heThongRap, iHeThongRap) => {
      return (
        <TabPane
          tab={<img src={heThongRap.logo} className="w-10 h-10" />}
          key={iHeThongRap}
        >
          <Tabs
            tabPosition={tabPosition}
            defaultActiveKey="0"
            style={{ height: heightTab, paddingLeft: 0 }}
          >
            {heThongRap.lstCumRap.map((cumRap, iCumRap) => {
              return (
                <TabPane
                  tab={
                    <div className="w-52 text-left pl-0 whitespace-nowrap text-ellipsis">
                      <p className=" text-green-600">{cumRap.tenCumRap}</p>
                      <p className=" text-gray-400">{cumRap.diaChi}</p>
                      <p className=" text-red-400">[Xem chi tiết]</p>
                    </div>
                  }
                  key={iCumRap}
                >
                  <div style={{ height: heightTab, overflowY: "scroll" }}>
                    {cumRap.danhSachPhim.map((movie, iMovie) => {
                      if (iMovie < 10) {
                        return <MovieTabItem movie={movie} key={iMovie} />;
                      }
                    })}
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };

  return (
    windowSize.width > 768 && (
      <>
        <Space
          style={{
            marginBottom: 24,
          }}
        ></Space>
        <Tabs tabPosition={tabPosition}>{renderTabContent()}</Tabs>
      </>
    )
  );
};

export default MovieTabs;
