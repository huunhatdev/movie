import React from "react";
import moment from "moment";
import { NavLink } from "react-router-dom";
import { useDispatch } from "react-redux";
import { onOpenTrailer } from "../../../redux/actions/userAction";
import { Tag } from "antd";

export default function MovieTabItem({ movie }) {
  const dispatch = useDispatch();
  return (
    <div className="flex border-b-2 py-5">
      <img src={movie.hinhAnh} alt="" className="w-28 h-40" />
      <div>
        <div className="ml-5">
          <div className=" items-center ">
            <div>
              {movie.hot ? <Tag color="red">Hot</Tag> : ""}
              {movie.sapChieu ? <Tag color="geekblue">Sắp chiếu</Tag> : ""}
              {movie.dangChieu ? <Tag color="green">Đang chiếu</Tag> : ""}
            </div>
            <NavLink to={"/detail/" + movie.maPhim}>
              <p className="text-xl inline-block my-2">{movie.tenPhim}</p>
            </NavLink>
          </div>
          <div className="">
            {movie.lstLichChieuTheoPhim.map((e, i) => {
              if (i < 20)
                return (
                  <NavLink to={"/purchase/" + e.maLichChieu}>
                    <div
                      className=" inline-block m-3 ml-0 border rounded-lg p-3 hover:font-bold w-44 hover:cursor-pointer "
                      style={{ transition: "0.1s" }}
                    >
                      <span className="text-green-600 ">
                        {moment(e.ngayChieuGioChieu).format("DD-MM-YYYY")}
                      </span>
                      <span>~</span>
                      <span className="text-red-500">
                        {moment(e.ngayChieuGioChieu).format("HH:MM")}
                      </span>
                    </div>
                  </NavLink>
                );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
