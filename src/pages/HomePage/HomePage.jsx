import _ from "lodash";
import React, { useEffect, useState } from "react";
import { movieService } from "../../services/movieService";
import MovieCarousel from "./MovieCarousel/MovieCarousel";
import "./homePage.css";
import MovieTabs from "./MovieTabs/MovieTabs";
import MovieBanner from "./MovieBanner/MovieBanner";
import { Spin } from "antd";
import MovieFooter from "./MovieFooter/MovieFooter";

export default function HomePage() {
  const [movieList, setMovieList] = useState([]);
  const [movieBanner, setMovieBanner] = useState([]);
  const [spin, setSpin] = useState(true);

  let fetchMovieList = async () => {
    let res = await movieService.getMovieList("GP03");
    let chunkedList = _.chunk(res.data.content, 8);
    setMovieList(chunkedList);
  };
  let fetchMovieBanner = async () => {
    let res = await movieService.getMovieBanner();
    setMovieBanner(res.data.content);
  };
  useEffect(() => {
    let fetchData = async () => {
      await fetchMovieList();
      await fetchMovieBanner();
    };
    fetchData();
    setSpin(false);
  }, []);

  return (
    <div className="relative">
      {movieList.length === 0 ? (
        <Spin
          size="large"
          spinning={spin}
          style={{
            position: "absolute",
            top: "50%",
            left: "50%",
            marginTop: "5rem",
          }}
        />
      ) : (
        <>
          <div id="banner_homePage" className="">
            <MovieBanner movieBanner={movieBanner} />
          </div>

          <div
            id="carousel_homePage"
            className="container mx-auto shadow-xl mb-20 pb-10 rounded-b-lg"
          >
            <MovieCarousel chunkedList={movieList} />
          </div>
          <div
            id="movieTab_homePage"
            className="container mx-auto mb-20  rounded-b-lg"
          >
            <MovieTabs />
          </div>
          <MovieFooter />
        </>
      )}
    </div>
  );
}
