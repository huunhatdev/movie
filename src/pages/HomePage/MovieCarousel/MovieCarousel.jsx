import { Carousel } from "antd";
import MovieItem from "../MovieItem/MovieItem";
const contentStyle = {
  height: "160px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};

const MovieCarousel = ({ chunkedList }) => (
  <Carousel>
    {chunkedList.map((movie, i) => {
      return (
        <div key={i} className="h-full w-full mx-auto p-10">
          <div className="grid lg:grid-cols-4 xl:gap-10 lg:gap-5 md:grid-cols-2 md:gap-5 sm:grid-cols-1 sm:gap-0">
            {movie.map((e, iItem) => {
              return <MovieItem key={iItem} movieItem={e} />;
            })}
          </div>
        </div>
      );
    })}
  </Carousel>
);

export default MovieCarousel;
