import { Carousel } from "antd";
import { useEffect } from "react";
import { movieService } from "../../../services/movieService";

const MovieBanner = ({ movieBanner }) => (
  <Carousel autoplay dots={true}>
    {movieBanner.map((e, i) => {
      return (
        <div className="">
          <img src={e.hinhAnh} alt="" className="" />
        </div>
      );
    })}
  </Carousel>
);

export default MovieBanner;
