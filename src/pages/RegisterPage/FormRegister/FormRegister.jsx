import { Button, Checkbox, Form, Input, message } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { setUserAction } from "../../../redux/actions/userAction";
import { localStorageService } from "../../../services/localStorageService";
import { userServices } from "../../../services/userService";

export default function FormRegister() {
  //can thiệp thanh url, dùng để chuyển hướng trang
  let history = useHistory();
  let dispacth = useDispatch();

  const onFinish = (values) => {
    userServices
      .postDangKi(values)
      .then((res) => {
        localStorageService.setUserInfo(res.data.content);
        dispacth(setUserAction(res.data.content));
        message.success("Đăng nhập kí tài khoản công");
        setTimeout(() => {
          history.push("/login");
        }, 2000);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  const onFinishFailed = (errorInfo) => {};

  return (
    <Form
      name="basic"
      layout="vertical"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label={<label style={{ color: "white" }}>Tên đăng nhập</label>}
        name="taiKhoan"
        rules={[
          {
            required: true,
            message: "Nhập tài khoản không bao gồm kí tự đặc biệt!",
            pattern: new RegExp(
              /^(?=[a-zA-Z0-9._]{4,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/
            ),
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        className=""
        label={<label style={{ color: "white" }}>Mật khẩu</label>}
        name="matKhau"
        rules={[
          {
            required: true,
            message:
              "Nhập mật khẩu bao gồm chữ thường, chữ in, số và kí tự đặc biệt!",
            pattern: new RegExp(
              /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/
            ),
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        className=""
        label={<label style={{ color: "white" }}>Email</label>}
        name="email"
        rules={[
          {
            required: true,
            message: "Nhập email!",
            pattern: new RegExp(
              /^[\w]+([\.-]?[\w]+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$$/
            ),
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        className=""
        label={<label style={{ color: "white" }}>Số điện thoại</label>}
        name="soDt"
        rules={[
          {
            required: true,
            message: "Nhập số điện thoại!",
            pattern: new RegExp(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/),
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        className=""
        label={<label style={{ color: "white" }}>Họ tên</label>}
        name="hoTen"
        rules={[
          {
            required: true,
            message: "Nhập họ và tên!",
            pattern: new RegExp(
              /^([A-VXYỲỌÁẦẢẤỜỄÀẠẰỆẾÝỘẬỐŨỨĨÕÚỮỊỖÌỀỂẨỚẶÒÙỒỢÃỤỦÍỸẮẪỰỈỎỪỶỞÓÉỬỴẲẸÈẼỔẴẺỠƠÔƯĂÊÂĐa-vxyỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđ]+)((\s{1}[A-VXYỲỌÁẦẢẤỜỄÀẠẰỆẾÝỘẬỐŨỨĨÕÚỮỊỖÌỀỂẨỚẶÒÙỒỢÃỤỦÍỸẮẪỰỈỎỪỶỞÓÉỬỴẲẸÈẼỔẴẺỠƠÔƯĂÊÂĐa-vxyỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđ]+){1,})$/
            ),
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item>
        <Button
          className=" h-10 mt-3 w-full"
          type="primary"
          htmlType="submit"
          block
          danger
        >
          Đăng kí
        </Button>
      </Form.Item>
      <Form.Item className="text-center text-white ">
        Đã có tài khoản?
        <a
          className=" text-blue-600 underline mx-2"
          onClick={() => {
            history.push("/login");
          }}
        >
          Quay lại đăng nhập
        </a>
      </Form.Item>
    </Form>
  );
}
