import React from "react";
import { NavLink } from "react-router-dom";
import FormRegister from "./FormRegister/FormRegister";
import "../css/styleLoginPage.css";

export default function RegisterPage() {
  return (
    <div className="wrapper-login w-screen h-full min-h-screen relative">
      <div className="bg-login w-screen h-full absolute"></div>
      <NavLink to={"/"} className="w-full flex z-10 h-28 relative">
        <img
          src="https://dehayf5mhw1h7.cloudfront.net/wp-content/uploads/sites/757/2022/03/26135251/Netflix-Brand-Logo.png"
          alt=""
        />
      </NavLink>
      <div className="form-login relative container mx-auto  p-5 rounded bg-black/50 z-10">
        <h2>Đăng kí</h2>
        <FormRegister />
      </div>
    </div>
  );
}
