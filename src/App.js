import logo from "./logo.svg";
// import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import { userRoutes } from "./routes/userRoutes";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          {userRoutes.map((route, i) => {
            if (route.isUseLayout) {
              return (
                <Route
                  path={route.path}
                  exact={route.exact}
                  // component={route.component}

                  render={() => {
                    return route.component;
                  }}
                />
              );
            }
            return (
              <Route
                path={route.path}
                exact={route.exact}
                component={route.component}
              />
            );
          })}
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
