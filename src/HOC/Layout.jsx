import React from "react";
import HeaderTheme from "../components/HeaderTheme/HeaderTheme";
import VideoOverplay from "../components/VideoOverplay/VideoOverplay";

export default function LayoutTheme({ Component }) {
  return (
    <div>
      <HeaderTheme />
      <Component />
      <VideoOverplay />
    </div>
  );
}
