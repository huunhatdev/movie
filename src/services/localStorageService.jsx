const USER = "USER";
export const localStorageService = {
  setUserInfo: (user) => {
    let dataJson = JSON.stringify(user);
    console.log(dataJson);
    localStorage.setItem(USER, dataJson);
  },
  getUserInfo: () => {
    let dataJson = localStorage.getItem(USER);
    console.log(dataJson);
    if (dataJson) {
      return JSON.parse(dataJson);
    }
    return null;
  },
  removeUserInfo: () => {
    localStorage.removeItem(USER);
  },
};
