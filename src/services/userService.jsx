import axios from "axios";
import { BASE_URL, httpService, TOKEN_CYBERSOFT } from "./configUrl";
import { localStorageService } from "./localStorageService";

export const userServices = {
  postDangNhap: (dataLogin) => {
    return httpService.post("/api/QuanLyNguoiDung/DangNhap", dataLogin);
  },
  postDangKi: (dataSignUp) => {
    return axios({
      method: "POST",
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
      data: { ...dataSignUp, maNhom: 2 },
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    });
  },

  postDatVe: (data) => {
    httpService.defaults.headers.post["Authorization"] =
      "Bearer " + localStorageService.getUserInfo().accessToken;
    return httpService.post("/api/QuanLyDatVe/DatVe", data);
  },
  getProfile: () => {
    httpService.defaults.headers.post["Authorization"] =
      "Bearer " + localStorageService.getUserInfo().accessToken;
    return httpService.post("/api/QuanLyNguoiDung/ThongTinTaiKhoan");
  },
  updateProfile: (data) => {
    httpService.defaults.headers.post["Authorization"] =
      "Bearer " + localStorageService.getUserInfo().accessToken;
    return httpService.post(
      "/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung",
      data
    );
  },
};
