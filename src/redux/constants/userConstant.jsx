export const SET_USER_INFO = "SET_USER_INFO";
export const USER_LOGOUT = "USER_LOGOUT";
export const SELECT_SEAT = "SELECT_SEAT";
export const CLEAR_SEAT = "CLEAR_SEAT";
export const ON_CLICK_LOGIN = "ON_CLICK_LOGIN";
export const ON_CLOSE_TRAILER = "ON_CLOSE_TRAILER";
export const ON_OPEN_TRAILER = "ON_OPEN_TRAILER";
