import {
  CLEAR_SEAT,
  ON_CLICK_LOGIN,
  ON_CLOSE_TRAILER,
  ON_OPEN_TRAILER,
  SELECT_SEAT,
  SET_USER_INFO,
  USER_LOGOUT,
} from "../constants/userConstant";
import { userServices } from "../../services/userService";
import { message } from "antd";

export const setUserAction = (user) => {
  return {
    type: SET_USER_INFO,
    payload: user,
  };
};
export const userLogoutAction = () => {
  return {
    type: USER_LOGOUT,
  };
};

export const setSelectSeat = (seatInfo, showtime) => {
  return {
    type: SELECT_SEAT,
    payload: {
      seatInfo,
      showtime,
    },
  };
};

export const clearSelect = () => {
  return {
    type: CLEAR_SEAT,
  };
};

export const onClickLogin = (pathname) => {
  return (dispatch) => {
    dispatch({
      type: ON_CLICK_LOGIN,
      payload: pathname,
    });
  };
};

export const onOpenTrailer = (url) => {
  let id = url.replace("https://www.youtube.com/embed/", "");
  return (dispatch) => {
    dispatch({
      type: ON_OPEN_TRAILER,
      payload: id,
    });
  };
};
export const onCloseTrailer = () => {
  return (dispatch) => {
    dispatch({
      type: ON_CLOSE_TRAILER,
    });
  };
};

export const setUserInfoActionServ = (
  dataLogin = {},
  handleSuccess = () => {},
  handlFail = () => {}
) => {
  return (dispatch) => {
    userServices
      .postDangNhap(dataLogin)
      .then((res) => {
        handleSuccess(res.data.content);
        dispatch({
          type: SET_USER_INFO,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        handlFail(err.response.data.content);
        // console.log("err", );
      });
  };
};
