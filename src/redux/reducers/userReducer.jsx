import { localStorageService } from "../../services/localStorageService";
import {
  CLEAR_SEAT,
  ON_CLICK_LOGIN,
  ON_CLOSE_TRAILER,
  ON_OPEN_TRAILER,
  SELECT_SEAT,
  SET_USER_INFO,
  USER_LOGOUT,
} from "../constants/userConstant";

const initialState = {
  userInfo: localStorageService.getUserInfo(),
  pathNameLogin: "",
  userSelect: [],
  trailer: {
    isOpen: false,
    id: "FUS_Q7FsfqU",
  },
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    //luu du lieu nguoi dung
    case SET_USER_INFO:
      state.userInfo = payload;
      return { ...state };
    //lôgout
    case USER_LOGOUT:
      state.userInfo = null;
      return { ...state };
    //lôgout
    case ON_CLICK_LOGIN:
      console.log(payload);
      state.pathNameLogin = payload;
      return { ...state };

    case SELECT_SEAT:
      let clone = [...state.userSelect];
      let i = -1;
      if (clone.length) {
        i = clone.findIndex((e) => {
          return e.maGhe === payload.seatInfo.maGhe;
        });
      }
      if (i === -1) {
        let json = JSON.stringify([...clone, payload.seatInfo]);
        return { ...state, userSelect: JSON.parse(json) };
      } else {
        clone.splice(i, 1);
        return {
          ...state,
          userSelect: JSON.parse(JSON.stringify(clone)),
        };
      }

    case CLEAR_SEAT:
      return { ...state, userSelect: [] };

    case ON_OPEN_TRAILER:
      state.trailer = { isOpen: true, id: payload };
      return { ...state };

    case ON_CLOSE_TRAILER:
      state.trailer = { isOpen: false, id: "" };
      return { ...state };

    default:
      return state;
  }
};
